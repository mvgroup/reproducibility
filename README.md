# Reproducibility workshop

This is a workshop about reproducibility for academic and industrial projects

![](presentation/assets/images/academy_sketch.png)


Find the presentation at [https://mvgroup.gitlab.io/mvlearn/workshop-reproducibility/presentation/index.html](https://mvgroup.gitlab.io/mvlearn/workshop-reproducibility/presentation/index.html)

Find the exercises of the workshop at [https://gitlab.com/mvgroup/mvlearn/workshop-reproducibility/-/tree/main/exercises](https://gitlab.com/mvgroup/mvlearn/workshop-reproducibility/-/tree/main/exercises)
