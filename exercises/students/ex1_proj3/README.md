# Drawing. How accurate can you get?

Just like a puzzle, use the components present on a canvas to form a final image based on the given description.


## Requirements

Open the file "_mask_decompose.drawio_" in [Draw.io](https://app.diagrams.net/) online software. No further installation required.

## Exercise

The individual components have only been rotated and/or scaled uniformly. When trying to change their size, make sure to always press SHIFT in order to mantain the proportions!

### Description

The final image depicts a simple and colorful form of an african / incas mask.

The circular mask is divided in two halves: 

The top depicts the face with a golden color with cream-white rays of light in a "sunshine-like" composition from the nose position towards the border of the face. The top half also contains the eyes which are shaped like pointed ovals with black eyeliners, white sclera and black circular eyeballs.

The bottom half, like the border of the mask, is of a dark-brown color and contains a mouth composed of red lips and a completely black teethless mouth. The mouth has a rounded rectangle shape. On the sides of the mouth, depicting the cheeks, there are two green pointed ovals with orange ellipses inside. There is no clearly depicted nose in the mask.


So, what did you get?