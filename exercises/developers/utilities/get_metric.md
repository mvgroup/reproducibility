# Get metric of reproducibility

## Instructions

1. Obtain the _generate_metric_from_input_paths.py_ file [here](https://gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises/utilities)
2. Execute the Python script:

```bash
$ python generate_metric_from_input_paths.py
```

3. The program will ask you to paste the path to two directories you want to compare.

```bash
$ Paste here path to parent folder of ORIGINAL project:
$ Paste here path to parent folder of REPRODUCIBLE project:
```

4. Copy paste sequentially the full path of the two directories and press Enter.
5. The program will return two reproducibility indeces: one for the folder and one for the files.
6. Send these values to the instructors.