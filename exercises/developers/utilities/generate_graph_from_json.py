import json
import pydot

f = open('edges.json')
edges_dict = json.load(f)

edges = edges_dict['edges']
nodes_ids = edges_dict['nodes_ids']

graph = pydot.Dot("my_graph", graph_type="graph")

for edge in edges:
    node1 = pydot.Node(edge[0],label=" "+nodes_ids[edge[0]], shape="folder", fillcolor="lemonchiffon", style="filled")
    node2 = pydot.Node(edge[1],label=" "+nodes_ids[edge[1]], shape="folder", fillcolor="lemonchiffon", style="filled")
    graph.add_node(node1)
    graph.add_node(node2)
    graph.add_edge(pydot.Edge(node1, node2))

#graph.write_png("output.png", prog=['neato', '-Goverlap=false'])
graph.write_png("output.png") # prog default 'dot'
#graph.write_raw('output_raw.dot')
print('Graph generated!')
