# Build a directory diagram

## Instructions

1. Obtain the _generate_json_from_input_path.py_ file [here](https://gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises/utilities)
2. Go to the directory you want to create the diagram for and copy the full path
3. Execute the Python script:

```bash
$ python generate_json_from_input_path.py
```

4. The program will ask you to paste the folder path you just copied in the terminal

```bash
$ Paste here path to parent folder of project:
```

5. Paste the directory path and press Enter
6. The script will generate inside the pasted directory a file called _edges.json_
7. Send the _edges.json_ file to the instructors

## Recommendations

- Avoid having a project that contains a large amount of nested folders (> 50).
- Avoid having a virtual environment or a .git folder inside the project.
- Avoid having folders that contains special characters ("%", "$", ";" etc..)