#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[45]:


import os

path = os.path.abspath(input("Paste here path to parent folder of ORIGINAL project:"))


if not os.path.exists(path):
    print('This is not a real path!')
    exit()


if path.endswith('/'):
    path = path[:-1]

alldirs1_ids = []
allfiles1_ids = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    if ("/." not in r):
        r = r.replace(path, "")
        alldirs1_ids.append(r)
        for file in f:
            allfiles1_ids.append(r+"/"+file)
    
# alldirs1_ids   
# allfiles1_ids



path = os.path.abspath(input("Paste here path to parent folder of REPRODUCIBLE project:"))



if not os.path.exists(path):
    print('This is not a real path!')
    exit()


if path.endswith('/'):
    path = path[:-1]

alldirs2_ids = []
allfiles2_ids = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    if ("/." not in r):
        r = r.replace(path, "")
        alldirs2_ids.append(r)
        for file in f:
            allfiles2_ids.append(r+"/"+file)
    
# alldirs2_ids   
# allfiles2_ids


# In[46]:


allfiles1_ids.sort()
allfiles2_ids.sort()


# In[47]:


common_files = [f for f in set(allfiles1_ids + allfiles2_ids) if f in allfiles1_ids and f in allfiles2_ids]
common_dirs = [f for f in set(alldirs1_ids + alldirs2_ids) if f in alldirs1_ids and f in alldirs2_ids]


# In[53]:


common_files_metric = len(common_files)/len(set(allfiles1_ids + allfiles2_ids))


# In[54]:


common_dirs_metric = len(common_dirs)/len(set(alldirs1_ids + alldirs2_ids))


# In[59]:


print("Reproducibility index (folder structure): {:.2f}".format(common_dirs_metric))
print("Reproducibility index (file existence): {:.2f}".format(common_files_metric))


# In[ ]:




