import os
import json

path = os.path.abspath(input("Paste here path to parent folder of project:"))

if not os.path.exists(path):
    print('This is not a real path!')
    exit()


if path.endswith('/'):
    path = path[:-1]

edges = []
alldirs_ids = {}
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    alldirs_ids[r] = os.path.split(r)[-1] # label
    
for r, d, f in os.walk(path):
    for directory in d:
        current_dir = os.path.join(r, directory)
        if ("/." not in current_dir) and (current_dir in alldirs_ids.keys()):
            edges.append([r, current_dir])

edges_dict = {'nodes_ids': alldirs_ids,
                'edges': edges}



with open(os.path.join(path, 'edges.json'), 'w') as f:
    json.dump(edges_dict, f)

print("You can find the files in the specified path!")

