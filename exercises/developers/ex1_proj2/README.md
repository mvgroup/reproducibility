# Textual analysis: What is the text talking about?

A semi-automatic textual analysis of a Wikipedia page. Manually adjust certain things and run the provided automatic analysis!
For this exercise, you'll need to perform 2 main tasks: A manual preprocessing of the text and a textual analysis

## Installation requirements

For this project, you'll need a few python packages. 

- If you're an expert in conda, create a conda environment and install the packages listed in the _requirements.txt_ file. 
- If you're not an expert in conda, just install the _requirements.txt_ packages from pip in your preferred environment or your default python installation path. 

## Exercise

### 1. Manual preprocessing of the input text:

In order to obtain a consistent input text for the textual analysis, the raw input (./input/ex1_proj2_raw.txt) needs to be corrected according to the following super easy riddles. 

Correct the text and save it in a file as './input/ex1_proj2_corrected.txt'.


#### Text manual preprocessing:

- `**1**`: (noun) vocal or instrumental sounds (or both) combined in such a way as to produce beauty of form, harmony, and expression of emotion
- `**2**`: (noun) brain disorder that slowly destroys memory and thinking skills and, eventually, the ability to carry out the simplest tasks
- `**3**`: (noun) a person receiving or registered to receive medical treatment.
- `**4**`: (noun) a strong, regular repeated pattern of movement or sound.
- What sentence(s) or paragraphs do not belong to the text? Delete them. 
- What disease is misspelled thoughout the text? Correct it. 
- What word is in Spanish? Translate it into English
- Once sentence has the vowels swapped by numbers, crack the code to correct it. 


### 2. Run the textual analysis notebook 

We'll see how close you can reproduce the output.
Run the notebook ./textual_analysis.ipynb 

Let's check the output!





