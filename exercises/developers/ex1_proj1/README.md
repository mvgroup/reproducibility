# Reproduce an automatic digital project

An actual simplified real-case-scenario project. You will attempt to understand and execute a project combining _ImageJ_ image analysis and Python data visualization.


## Installation requirements

### 1. ImageJ

- Download Fiji (ImageJ) can be from [here](https://imagej.net/software/fiji/downloads)
- Unzip the downloaded folder
- Open the software by clicking ./Fiji.app/ImageJ-win64.exe (Windows) or ./Fiji.app/ImageJ-linux64 (Linux). No further installation is required.

### 2. Python

To use the Python code the use of **miniconda virtual environment** is recommended.

Download and install miniconda following [the instructions](https://docs.conda.io/en/latest/miniconda.html), then simply create a python virtual environment with all the required packages using from the terminal/anaconda shell:

```
$ conda env create -f env.yml 
```

This will create an environment called workshop_env. To activate it:

```
$ conda activate workshop_env
```

## Exercise

Check the output folder(s). If they are not empty, please do so. 

### 1. Image processing

- Drag and drop the .ijm script inside the window of the Fiji desktop app. 
- Click _Run_. Select the folder pointing at **ex1_proj1/**. You should see some images appearing and if everything went fine no errors should appear and some output should have been stored.

### 2. Run Python scripts

With the conda environment activated, run the other 2 python scripts in the given order. 
Do so with your prefer python interface tool (command line, VS code, jupyter notebook, etc.)

_Example from command line_:

```
# with workshop_env activated

$ python 02_DescriptiveStatistics.py
$ python 03_PlotMeasurements.py

```

Let's check the output!


## References

### Source

Material originally from [here](https://gitlab.com/igbmc/mic-photon/workshop-version-control).


