// Example macro to segment and analyze particles

// Choose a folder to set up relative path references
path = getDir("Select repository folder (workshop-version-control)");
// Open image
OpenImage(path)
// Get window name since all functions refer to it
image_title = getTitle();
// Duplicate original image to create binary thresholded image
run("Duplicate...", "title=thresholded");
// Create binary mask
ThresholdBinary("thresholded");
// Measure particles params
MeasureParticles(image_title, 20, path);
// Create overlay image to see detected particles and number
SaveOverlay(image_title, path);
// Close all
cleanUp();


// Functions definition

function OpenImage(path) { 
	// Open blobs image
	open(path+"/data/blobs.tif");
}

function ThresholdBinary(WindowName){
	// Creates binary mask using auto threshold with black background
	selectWindow(WindowName);
	setAutoThreshold("Default no-reset");
	setOption("BlackBackground", true);
	run("Convert to Mask");
}

function MeasureParticles(WindowName, min_size, path) { 
	// Calculates some parameters for each particle bigger than min_size and save them to disk
	run("Set Measurements...", "area mean perimeter display redirect="+WindowName+" decimal=3");
	run("Analyze Particles...", "size="+min_size+"-Infinity display add");
	saveAs("Results", path+"/output/measurements/measurements.csv");
}

function SaveOverlay(WindowName, path) { 
	// Generate overlay and save it to disk
	selectWindow(WindowName);
	roiManager("Show All");
	run("Flatten");
	
	saveAs("PNG", path+"/output/segmentation/segmentation_overlay.png");
}

function cleanUp() {
// adapted from https://imagej.nih.gov/ij/macros/CloseAllWindows.txt
// Closes the "Results" and "Log" windows and all image windows

    requires("1.30e");
    if (isOpen("Results")) {
         selectWindow("Results"); 
         run("Close" );
    {
    if (isOpen("Log")) {
         selectWindow("Log");
         run("Close" );
    }
    while (nImages()>0) {
          selectImage(nImages());  
          run("Close");
    }
    
	if (isOpen("ROI Manager")) {
	 selectWindow("ROI Manager");
	 run("Close");
	}
}
