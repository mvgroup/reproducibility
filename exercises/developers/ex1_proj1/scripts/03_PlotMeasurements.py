# Import packages
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

# Define paths
measurements_path = "../output/measurements/"
figures_path = "../output/figures/"

# Import data
file_name = "measurements.csv"
df = pd.read_csv(measurements_path+file_name)

# Drop useless columns
df_drop = df.drop(df.columns[[0,1]], axis=1)

# Generate 3 equally-sized categories of particle area
cuts = pd.cut(df_drop["Area"], 3, labels=['small', 'medium', 'large'])
df_drop["Particle_Cat"] = cuts

# Create a nice figure
## set good-looking style using seaborn
sns.set(context='poster', style='ticks')
## create figure and axes objects with an arbitrary large size
f, axes = plt.subplots(2,2, figsize=(20,16))

# Linear regression line relationship between area and perimeter of particles
sns.regplot(data=df_drop, x="Area", y="Perim.", ax=axes.ravel()[0])

# Violin plot of mean values by area category
sns.violinplot(data=df_drop, x="Particle_Cat", y='Mean', ax=axes.ravel()[1])

# Histogram of mean intensities with density curve
sns.histplot(data=df_drop, x="Mean", kde=True, ax=axes.ravel()[2])

# Density plots of mean intensity distribution by area category
sns.kdeplot(data=df_drop, x="Mean", hue="Particle_Cat", fill=False, linewidth=3, ax=axes.ravel()[3])

# Adjust spacings automagically
plt.tight_layout()
# Save figure to disk
plt.savefig(figures_path+"exploratory_plots.pdf")
# Close figure after saving
plt.close()
