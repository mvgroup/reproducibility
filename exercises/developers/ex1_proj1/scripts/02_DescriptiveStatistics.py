# Import packages
import pandas as pd
from matplotlib import pyplot as plt

# Define paths
measurements_path = "../output/measurements/"

# Import data
file_name = "measurements.csv"
df = pd.read_csv(measurements_path+file_name)
# Drop useless columns
df_drop = df.drop(df.columns[[0,1]], axis=1)

# Save some descriptive statistics of particles
df_describe = df_drop.describe()
df_describe.to_csv(measurements_path+"descriptive_statistics.csv", index=True)
