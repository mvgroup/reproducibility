##################################################################################################

##			 TELEMETRIA PROYECTO. PREPARACION DE DATOS / HERRAMIENTA DE VISUALIZACION

##################################################################################################


## Section 0. Import packages
import os
import tkinter as tk
from tkinter import *
from tkinter import ttk #css for tkinter allows to make
from tkinter.filedialog import LoadFileDialog
from tkinter import messagebox as msg
import matplotlib
matplotlib.use('TkAgg') #backend of matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import matplotlib.figure
import matplotlib.animation as animation
import pandas as pd
from datetime import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
from matplotlib import style
import scipy.stats as stats
import statsmodels.api as sm
import seaborn as sns


## Section 1. Import raw data and preprocessing

def MakeData(file, time_value, time_type):
	try: time_value = float(time_value)
	except ValueError:msg.showerror('Data type Error','Please enter a valid number') # error messagebox, etc
	else:
		names_data=["ID","FECHA","GRUPO","GAS","LUZ","TEMP","HUM"]
		time_freq_ww=str(time_value)+str(time_type)

		df = pd.read_csv(file, delimiter=',')
		df.columns=names_data
		df['FECHA'] =  pd.to_datetime(df['FECHA'], format='%d-%m-%y %H:%M')
		start = df['FECHA'].min()
		end = df['FECHA'].max()

		datelist = pd.date_range(start=start, end=end,freq=time_freq_ww)

		array = datelist<=df['FECHA'][100]

		interval_list = []

		for date in df['FECHA']:
		    array = datelist<=date
		    interval_list.append(datelist[array.sum()-1])
		   

		df['FECHA_INTERVALO'] = pd.Series(interval_list, dtype='category')
		df.head()

		grouped = df.groupby(df['FECHA_INTERVALO'],as_index=False)

		TM = grouped.mean().drop('ID',axis=1)
		TM['HORA'] = pd.Series([x.hour for x in TM['FECHA_INTERVALO']])

		TM['FECHA']=pd.to_datetime(TM['FECHA_INTERVALO'])

		jornada_list=[]
		for date in TM['HORA']:

		    if date>12 and date<=18:
		        array="TARDE"
		    else:
		        if date>=6 and date<=12:
		            array="MANANA"
		        else:
		            array="NOCHE"
		    jornada_list.append(array)
		    
		TM['JORNADA']=pd.Series(jornada_list)
		return TM

def load_file(fname, timevalue, timetype):
	#app.frames[StartPage].LoadorNot.set('Loading')
	global TM
	TM = MakeData(str(fname), timevalue, timetype)
	app.frames[StartPage].LoadorNot.set('File Loaded')
	PageGraph_stateofthedata.updateplot(app.frames[PageGraph_stateofthedata],app.frames[PageGraph_stateofthedata].a,
		app.frames[PageGraph_stateofthedata].b,app.frames[PageGraph_stateofthedata].c,app.frames[PageGraph_stateofthedata].d,
		app.frames[PageGraph_stateofthedata].f)
	PageGraph_heatmap.updateplot(app.frames[PageGraph_heatmap], app.frames[PageGraph_heatmap].a,
		app.frames[PageGraph_heatmap].b, app.frames[PageGraph_heatmap].c, app.frames[PageGraph_heatmap].f)
	#win.destroy()
	return TM

LARGE_FONT = ('Verdana', 12)
style.use('ggplot')


class DataVis(tk.Tk):

	def __init__(self, *args, **kwargs):
		tk.Tk.__init__(self, *args, **kwargs)

		#tk.Tk.iconbitmap(self, default='wifi.ico')
		tk.Tk.wm_title(self, 'CO2 Telemetria')

		container = tk.Frame(self)
		container.pack(side='top', fill='both', expand=True)
		container.grid_rowconfigure(0, weight=1)
		container.grid_columnconfigure(0, weight=1)

		self.frames = {}
		for F in (StartPage, PageOne,PageGraph_stateofthedata,PageGraph_relationvisualization,PageGraph_heatmap):
			frame = F(container, self)
			self.frames[F] = frame
			frame.grid(row=0, column=0, sticky='nsew') #north south east west = we stretch the frame in all the window

		self.show_frame(StartPage)

	def show_frame(self, cont):
		frame = self.frames[cont]
		frame.tkraise() #raise the window to the front
	
class StartPage(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		label = tk.Label(self, text='Welcome to Telemetria Visualization App', font=('Verdana',22))
		label.pack(pady=10,padx=10)
		label1 = tk.Label(self, text='Choose a file to load', font=LARGE_FONT)
		label1.pack(pady=10, padx=10)
		label2 = tk.Label(self, text='The app will read only .csv files.\nMake sure that the file you want to load is in the same directory of this app.\nThe loading may take a while depending on the file size', font=('Verdana',10))
		label2.pack(pady=5, padx=5)

		fname = StringVar()
		file_list = [doc for doc in os.listdir() if doc.endswith(".csv")]
		selection = ttk.OptionMenu(self, fname, file_list[0], *file_list)
		selection.config(width=30)
		selection.pack()

		label3 = tk.Label(self, text='Enter a time window width.\nEnter a number in the box and a time type below. Then press Load File and wait until "File Loaded" appears')
		label3.pack(pady=10,padx=10)
		TimeWindowValue = StringVar()
		TimeWindowEntry = ttk.Entry(self, textvariable=TimeWindowValue)
		TimeWindowEntry.config(width=10)
		TimeWindowEntry.pack()
		TimeWindowType = StringVar()
		TimeWindowOption = ttk.OptionMenu(self, TimeWindowType, "min", "s","min","H","D")
		TimeWindowOption.pack()

		buttonOK = ttk.Button(self, text='Load File', command=lambda: load_file(fname.get(), TimeWindowValue.get(), TimeWindowType.get()), width=30)
		buttonOK.pack(pady=10)
		self.LoadorNot=StringVar()
		labelLoad = tk.Label(self,textvariable=self.LoadorNot, font=LARGE_FONT)
		labelLoad.pack(pady=2,padx=2)

		button1 = ttk.Button(self, text='Start',
			command = lambda: self.reset_load(PageOne), width=50)
		button1.pack()

		button2 = ttk.Button(self, text='Quit',
			command = quit, width=50)
		button2.pack()

	def reset_load(self, Page):
		app.frames[StartPage].LoadorNot.set('');app.show_frame(Page)

		
class PageOne(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		label = tk.Label(self, text='INSTRUCTIONS:', font=LARGE_FONT)
		label.pack(pady=10,padx=10)
		label1 = tk.Label(self, text='Press the buttons to navigate the interface', font=LARGE_FONT)
		label1.pack(pady=10,padx=10)

		button1 = ttk.Button(self, text='Back to Home',
			command = lambda: controller.show_frame(StartPage), width=30)
		button1.pack()
		button2 = ttk.Button(self, text='Status of the Data',
			command = lambda: controller.show_frame(PageGraph_stateofthedata), width=30)
		button2.pack()
		button3 = ttk.Button(self, text='Relationships',
			command = lambda: controller.show_frame(PageGraph_relationvisualization), width=30)
		button3.pack()
		button4 = ttk.Button(self, text='Correlation Heatmaps',
			command = lambda: controller.show_frame(PageGraph_heatmap), width=30)
		button4.pack()
		

class PageGraph_stateofthedata(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		label = tk.Label(self, text='The status of the data', font=('Verdana', 20))
		label.pack(pady=10,padx=10)

		button1 = ttk.Button(self, text='HomePage',
			command = lambda: controller.show_frame(StartPage), width=30)
		button1.pack(pady=1)
		button2 = ttk.Button(self, text='Relationships',
			command = lambda: controller.show_frame(PageGraph_relationvisualization), width=30)
		button2.pack(pady=1)
		button3 = ttk.Button(self, text='Correlation Heatmap',
			command = lambda: controller.show_frame(PageGraph_heatmap), width=30)
		button3.pack(pady=1)

		self.f = plt.figure()
		self.f.subplots_adjust(left=0.08, bottom=0.20, right=0.97, top=0.94,
                    wspace=0.13, hspace=0.04)
		self.a = plt.subplot2grid((2,2), (0,0), rowspan=1, colspan=1)
		self.b = plt.subplot2grid((2,2), (0,1), rowspan=1, colspan=1)
		self.c = plt.subplot2grid((2,2), (1,0), rowspan=1, colspan=1, sharex=self.a)
		self.d = plt.subplot2grid((2,2), (1,1), rowspan=1, colspan=1, sharex=self.b)

		canvas = FigureCanvasTkAgg(self.f, self)
		canvas.show()
		canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

		toolbar = NavigationToolbar2TkAgg(canvas, self)
		toolbar.update()
		canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

	def updateplot(self,a,b,c,d,f):
		a.clear()
		b.clear()
		c.clear()
		d.clear()
		self.plotdata(a,b,c,d,f)
		f.canvas.draw()

	def plotdata(self,a,b,c,d,f):
		## Section 2.1 Status of the data
		x_var = 'FECHA'
		y_var1 = 'GAS'
		y_var2 = 'LUZ'
		y_var3 = 'HUM'
		y_var4 = 'TEMP'
		start_plot=TM['FECHA'].min()
		end_plot=TM['FECHA'].max()
		time_freq_ww_plot='4H'

		label_dates = pd.date_range(start=start_plot, end=end_plot, freq=time_freq_ww_plot)
		label_objs = []
		for label_date in label_dates:
			label_objs.append((label_date.month,label_date.day,label_date.hour))

		a.plot_date(TM[x_var],TM[y_var1],'g')
		a.set_xlabel(x_var)
		a.set_ylabel(y_var1)
		a.xaxis.set_ticks(pd.date_range(start=start_plot, end=end_plot, freq=time_freq_ww_plot))
		a.xaxis.set_ticklabels(["{}-{}-{}".format(obj[0],obj[1], obj[2]) for obj in label_objs])
		a.xaxis.set_tick_params(labelsize=6)

		#plt.title('The status of the data')

		
		b.plot_date(TM[x_var],TM[y_var2],'r')
		b.set_xlabel(x_var)
		b.set_ylabel(y_var2)
		b.xaxis.set_ticks(pd.date_range(start=start_plot, end=end_plot, freq=time_freq_ww_plot))
		b.xaxis.set_ticklabels(["{}-{}-{}".format(obj[0],obj[1], obj[2]) for obj in label_objs])
		b.xaxis.set_tick_params(labelsize=6)
		

		
		c.plot_date(TM[x_var],TM[y_var3],'b')
		c.set_xlabel(x_var)
		c.set_ylabel(y_var3)
		c.xaxis.set_ticks(pd.date_range(start=start_plot, end=end_plot, freq=time_freq_ww_plot))
		c.xaxis.set_ticklabels(["{}-{}-{}".format(obj[0],obj[1], obj[2]) for obj in label_objs])
		c.xaxis.set_tick_params(labelsize=6)
		

		
		d.plot_date(TM[x_var],TM[y_var4],'k')
		d.set_xlabel(x_var)
		d.set_ylabel(y_var4)
		d.xaxis.set_ticks(pd.date_range(start=start_plot, end=end_plot, freq=time_freq_ww_plot))
		d.xaxis.set_ticklabels(["{}-{}-{}".format(obj[0],obj[1], obj[2]) for obj in label_objs])
		d.xaxis.set_tick_params(labelsize=6)
		f.autofmt_xdate(rotation=90) 

	
		#a.legend(bbox_to_anchor=(0,1.02,1,.102),loc=3, ncol=2, borderaxespad=0)
		#a.axes.get_xaxis().set_visible(False)
		#b.axes.get_xaxis().set_visible(False)
		#c.axes.get_xaxis().set_visible(False)
		#d.axes.get_xaxis().set_visible(False)


class PageGraph_relationvisualization(tk.Frame):

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		label = tk.Label(self, text='Data relationships', font=('Vardana', 20))
		label.pack(pady=10,padx=10)
		label1 = tk.Label(self, text='Select variables and click Update', font=LARGE_FONT)
		label1.pack(pady=10,padx=10)

		button1 = ttk.Button(self, text='HomePage',
			command = lambda: controller.show_frame(StartPage), width=30)
		button1.pack(side='top')
		button2 = ttk.Button(self, text='Status of the data',
			command = lambda: controller.show_frame(PageGraph_stateofthedata), width=30)
		button2.pack(side='top')
		button3 = ttk.Button(self, text='Correlation Heatmap',
			command = lambda: controller.show_frame(PageGraph_heatmap), width=30)
		button3.pack(side='top')

		## Section 2.2 Relation between variables

		holder = ttk.Frame(self, width=10, height=20)
		holder.pack(side="left")
		labelx = tk.Label(holder, text='Variable X', font=LARGE_FONT)
		labelx.pack()

		x_var = StringVar()
		xvar_select = ttk.OptionMenu(holder, x_var, "LUZ", "LUZ","GAS","TEMP","HUM", "HORA")
		xvar_select.pack()
		labelx = tk.Label(holder, text='Variable Y', font=LARGE_FONT)
		labelx.pack()
		y_var = StringVar()
		
		yvar_select = ttk.OptionMenu(holder, y_var, "LUZ", "LUZ","GAS","TEMP","HUM", "HORA")
		yvar_select.pack()

		buttonUpdate = ttk.Button(holder,text='Update Plot', command= lambda: self.updateplot(x_var.get(),y_var.get(), ax, fig))
		buttonUpdate.pack()

		fig = plt.figure(figsize=(5,3), dpi=100)
		fig, ax = plt.subplots(1,1)
		fig.subplots_adjust(left=0.08, bottom=0.11, right=0.98, top=0.97,
                    wspace=0.20, hspace=0.20)


		canvas = FigureCanvasTkAgg(fig, self)
		canvas.show()
		canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

		toolbar = NavigationToolbar2TkAgg(canvas, self)
		toolbar.update()
		canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)


	def updateplot(self, x, y, a, f):
		a.clear()
		self.makeplot(x, y, a)
		f.canvas.draw()

	def makeplot(self, x, y, a):
		frac_points=0.4
		group_jornada = TM.groupby("JORNADA")

		lowess = sm.nonparametric.lowess
		for name,group in group_jornada:
		        a.scatter(group_jornada.get_group(name)[x], group_jornada.get_group(name)[y], label=name, s=10)
		        #z=lowess(group_jornada.get_group(name)[y],group_jornada.get_group(name)[x])
		        #a.plot(z[0:,0],z[0:,1])

		z=lowess(TM[y],TM[x])
		a.plot(z[0:,0],z[0:,1],c="black")
		a.legend(loc=1)
		a.set_xlabel(x)
		a.set_ylabel(y)

	
class PageGraph_heatmap(tk.Frame):

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		label = tk.Label(self, text='Correlation Heatmaps', font=LARGE_FONT)
		label.pack(pady=10,padx=10)

		button1 = ttk.Button(self, text='HomePage',
			command = lambda: controller.show_frame(StartPage), width=30)
		button1.pack()
		button2 = ttk.Button(self, text='Relationships',
			command = lambda: controller.show_frame(PageGraph_relationvisualization), width=30)
		button2.pack()
		button3 = ttk.Button(self, text='Status of the data',
			command = lambda: controller.show_frame(PageGraph_stateofthedata), width=30)
		button3.pack()

		self.f = plt.figure(figsize=(7,6), dpi=100)
		self.a=plt.subplot2grid((5,1), (0,0), rowspan=1, colspan=1)
		self.b=plt.subplot2grid((5,1), (2,0), rowspan=1, colspan=1)
		self.c=plt.subplot2grid((5,1), (4,0), rowspan=1, colspan=1)

		canvas = FigureCanvasTkAgg(self.f, self)
		canvas.show()
		canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

		toolbar = NavigationToolbar2TkAgg(canvas, self)
		toolbar.update()
		canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
		## Section 2.3 Correlation heatmap 

	def updateplot(self, a,b,c,f):
		a.clear()
		b.clear()
		c.clear()
		self.heatmapplot(a,b,c)
		f.canvas.draw()

	def heatmapplot(self, a,b,c):

		group_jornada = TM.drop(["FECHA_INTERVALO", "GRUPO", "FECHA"],axis=1).groupby("JORNADA")

		
		a=plt.subplot2grid((5,1), (0,0), rowspan=1, colspan=1)
		sns.heatmap(group_jornada.get_group('MANANA').corr(), annot=True)
		plt.title("MANANA")
		b=plt.subplot2grid((5,1), (2,0), rowspan=1, colspan=1)
		sns.heatmap(group_jornada.get_group('TARDE').corr(), annot=True)
		plt.title("TARDE")
		c=plt.subplot2grid((5,1), (4,0), rowspan=1, colspan=1)
		sns.heatmap(group_jornada.get_group('NOCHE').corr(), annot=True)
		plt.title("NOCHE")
		

app = DataVis()
w, h = app.winfo_screenwidth(), app.winfo_screenheight()
#app.overrideredirect(1)
app.geometry("%dx%d+0+0" % (w, h))
app.focus_set()
app.mainloop()