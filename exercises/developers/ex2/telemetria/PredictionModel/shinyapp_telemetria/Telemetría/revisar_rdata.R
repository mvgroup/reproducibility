### Revisar el archivo .RData
## Reemplaza acá el directorio donde está el archivo .RData
setwd("~/Google Drive/Independent work/Andres Gomez/PredictionModel/shinyapp_telemetria/Telemetría")
## Este comando carga el archivo, cuando lo cargues verás en la parte derecha que se cargan unos objetos
load("telemetria_modelos_prediccion_voting_final_v2.RData")

#Estos son los objetos. "names" te dice los nombres de los objetos que hay dentro de un objeto"
names(models_list) 
#Acá ves cada uno de los objetos que son los tres modelos.
#Cada uno de estos objetos cuando lo corres te imprime un resumen del modelo que tiene unas medidas de error como las que te puse en el dashboard
#Sin embargo el objeto contiene todos los elementos correspondientes a la estimación. Todos esos objetos los puedes ver con la función str(), pero no es tan básico de entender, creo.
models_list$MODEL_1
models_list$MODEL_2
models_list$MODEL_3
str(models_list$MODEL_1)
#Acá tienes los training sets de cada modelo
names(TM_TRAIN_list)
TM_TRAIN_list$MODEL_1
TM_TRAIN_list$MODEL_2
TM_TRAIN_list$MODEL_3
#Acá tienes los test set de cada modelo
names(TM_TEST_list)
TM_TEST_list$MODEL_1
TM_TEST_list$MODEL_2
TM_TEST_list$MODEL_3




