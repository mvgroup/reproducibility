
##################################################################################################

##			 TELEMETRIA PROYECTO. PREPARACION DE DATOS / HERRAMIENTA DE VISUALIZACION

##################################################################################################


## Section 0. Import packages

import pandas as pd
from datetime import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
from matplotlib import style
import seaborn as sns

## Section 1. Import raw data and preprocessing

names_data=["ID","FECHA","GRUPO","GAS","LUZ","TEMP","HUM"]
time_freq_ww='4min'

df = pd.read_csv('TM1.csv', delimiter=',', names=names_data)
df['FECHA'] =  pd.to_datetime(df['FECHA'], format='%Y-%m-%d %H:%M:%S')
start = df['FECHA'].min()
end = df['FECHA'].max()

datelist = pd.date_range(start=start, end=end,freq=time_freq_ww)

array = datelist<=df['FECHA'][100]

interval_list = []

for date in df['FECHA']:
    array = datelist<=date
    interval_list.append(datelist[array.sum()-1])
   

df['FECHA_INTERVALO'] = pd.Series(interval_list, dtype='category')
df.head()

grouped = df.groupby(df['FECHA_INTERVALO'],as_index=False)

TM = grouped.mean().drop('ID',axis=1)
TM['HORA'] = pd.Series([x.hour for x in TM['FECHA_INTERVALO']])

TM['FECHA']=pd.to_datetime(TM['FECHA_INTERVALO'])

## Section 2. Data Visualization

## Section 2.1 State of the data
style.use('ggplot')

x_var = 'FECHA'
y_var = 'GAS'

plt.plot_date(TM[x_var],TM[y_var],'g')
plt.xlabel(x_var)
plt.ylabel(y_var)
plt.show()
