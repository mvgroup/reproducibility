##########################################################

##### TELEMETRIA, CONDICIONES DE CO2

##########################################################

rm(list=ls())

## Install packages

packs<-c("data.table", "ggplot2", "caret", "brnn")
#install.packages(packs, dependencies = T)

## Set working directory

setwd("/home/valeria/vfonsecad/independent-work/andres-gomez")


## Load packages

library(data.table)
library(ggplot2)
library(caret)
library(brnn)


## Load and prepare data
ntunes<-c(5,8,10,20,30)
tune_results<-matrix(NA,length(ntunes),7)

for(ii in 1:length(ntunes)){
  print(ii)
TM<-na.omit(fread("TM24.csv"))

colnames(TM)<-toupper(colnames(TM))
vars_selected<-c("GAS", "LUZ", "TEMPERATURA", "HUMEDAD")



secs_interval<-60*ntunes[ii] #4 minutes=300 sec
x_initial<-min(as.POSIXlt(TM$FECHA, format="%d-%m-%y %H:%M"))
x_final<-max(as.POSIXlt(TM$FECHA, format="%d-%m-%y %H:%M"))
x_interval<-seq(from=x_initial,to=x_final+2*secs_interval, by=secs_interval) 
x_interval_groups<-unlist(lapply(1:dim(TM)[1], function(x) which(as.POSIXlt(TM[x,FECHA],format="%d-%m-%y %H:%M")<=x_interval)[1]))


TM$FECHA_INTERVALO<-x_interval[x_interval_groups]
TM<-TM[, lapply(.SD, mean), by=FECHA_INTERVALO, .SDcols=vars_selected]

TM$HORA<-hour(as.ITime(as.POSIXlt(TM$FECHA_INTERVALO)))
TM[,c("JORNADA"):=list(ifelse(HORA<6|HORA>18, "NOCHE",ifelse(HORA>=6&HORA<=12,"MANANA", "TARDE")))]
TM[,ID:=as.character(1:.N)]
fecha_sort<-sort.int(TM$FECHA_INTERVALO, index.return = T)
TM<-TM[fecha_sort$ix]


## PREDICTION MODEL



## Dividir DATASET 

jornadas<-unique(TM[,JORNADA])


TM_VAL<-rbindlist(lapply(jornadas, function(x){
  x2<-TM[JORNADA==x]
  x2[sample(.N, round(0.1*.N))]}))

TM_TEST<-rbindlist(lapply(jornadas, function(x){
  x2<-fsetdiff(TM, TM_VAL)[JORNADA==x]
  x2[sample(.N, round(0.1*.N))]}))

TM_TRAIN<-fsetdiff(fsetdiff(TM,TM_VAL),TM_TEST)

#sum(dim(TM_TRAIN)[1],dim(TM_VAL)[1],dim(TM_TEST)[1])


TM_TRAIN<-TM_TRAIN[,lapply(.SD, function(x) (x-mean(x))/sd(x)),.SDcols=c(vars_selected,"HORA")]
TM_VAL<-TM_VAL[,lapply(.SD, function(x) (x-mean(x))/sd(x)),.SDcols=c(vars_selected,"HORA")]
TM_TEST<-TM_TEST[,lapply(.SD, function(x) (x-mean(x))/sd(x)),.SDcols=c(vars_selected,"HORA")]


## BRNN 

# Fit  model using 10 x 1-fold CV: BRNN
#normalize=T, activation function is tanh, cost function=betaEd+alphaEw, Ed=SSE, Ew=SSw

model<-train(GAS ~ ., TM_TRAIN,method = "brnn",
             trControl = trainControl(
               method = "cv", number =10,
               repeats = 1, verboseIter = TRUE),
             tuneGrid=expand.grid(neurons=1:10)
             )
model
p<-predict(model,TM_TRAIN)
plot(TM_TRAIN$GAS,p)
boxplot(residuals(model))
#hist(residuals(model))
p1<-predict(model,TM_VAL)
plot(TM_VAL$GAS,p1);
rmse_val<-sqrt(mean((TM_VAL$GAS-p1)^2))
p2<-predict(model,TM_TEST)
plot(TM_TEST$GAS,p2);
rmse_test<-sqrt(mean((TM_TEST$GAS-p2)^2))

id_neurons<-which(model$results$neurons==as.numeric(model$bestTune))[1]
tune_results[ii,]<-as.matrix(cbind(model$results[id_neurons,],rmse_val, rmse_test))

}

tune_results<-as.data.frame(tune_results)

##########################################################
##TRAINING BASED ON DIVISION ACCORDING TO TIME
# Tuning parameters SVM
tune_sigma<-seq(0.001,2,(2-0.001)/50)
tune_ww<-c(10)
crossval_results<-matrix(NA,length(tune_ww), length(tune_sigma))

for(ii in 1:length(tune_ww)){
  ## Load data
    print(ii)
TM<-na.omit(fread("TM21.csv"))

## Prepare data
colnames(TM)<-toupper(colnames(TM))
vars_selected<-c("GAS", "LUZ", "TEMPERATURA", "HUMEDAD")
secs_interval<-60*tune_ww[ii] #4 minutes=300 sec
x_initial<-min(as.POSIXlt(TM$FECHA, format="%d-%m-%y %H:%M"))
x_final<-max(as.POSIXlt(TM$FECHA, format="%d-%m-%y %H:%M"))
x_interval<-seq(from=x_initial,to=x_final+2*secs_interval, by=secs_interval) 
x_interval_groups<-unlist(lapply(1:dim(TM)[1], function(x) which(as.POSIXlt(TM[x,FECHA],format="%d-%m-%y %H:%M")<=x_interval)[1]))

TM$FECHA_INTERVALO<-x_interval[x_interval_groups]
TM<-TM[, lapply(.SD, mean), by=FECHA_INTERVALO, .SDcols=vars_selected]
TM$HORA<-hour(as.ITime(as.POSIXlt(TM$FECHA_INTERVALO)))
TM[,c("JORNADA"):=list(ifelse(HORA<6|HORA>18, "NOCHE",ifelse(HORA>=6&HORA<=12,"MANANA", "TARDE")))]
TM[,ID:=as.character(1:.N)]
fecha_sort<-sort.int(TM$FECHA_INTERVALO, index.return = T)
TM<-TM[fecha_sort$ix]

## Divide training and test data

id_train<-round(dim(TM)[1]*0.8)
#plot(TM[, FECHA_INTERVALO], TM[,GAS], "l")
#lines(TM[id_train:.N, FECHA_INTERVALO], TM[id_train:.N,GAS], col="red")

TM_TRAIN2<-TM[1:id_train,lapply(.SD, function(x) (x-mean(x))/sd(x)),.SDcols=c(vars_selected,"HORA")]
#TM_TEST2<-TM[id_train:.N,lapply(.SD, function(x) (x-mean(x))/sd(x)),.SDcols=c(vars_selected,"HORA")]

## Fit Model
for(jj in 1:length(tune_sigma)){
print(jj)
#model<-train(GAS ~ ., TM_TRAIN2,method = "brnn",
#             trControl = trainControl(
#               method = "cv", number =10,
#               repeats = 1, verboseIter = TRUE),
#             tuneGrid=expand.grid(neurons=10)
#)
#model<-lm(GAS~., data = TM_TRAIN2)
model<-gausspr(GAS ~ ., data=TM_TRAIN2, type="regression", 
               kernel="rbfdot",kpar=list(sigma=tune_sigma[jj]), 
               variance.model=F, cross=5) 
model
#crossval_results[ii,jj]<-model@cross
#p<-predict(model,TM_TRAIN2)
#plot(TM_TRAIN2$GAS,p);sqrt(mean((TM_TRAIN2$GAS-p)^2))
#p1<-predict(model,TM_TEST2)
#plot(TM_TEST2$GAS,p1);sqrt(mean((TM_TEST2$GAS-p1)^2))



}
}


##########################################################
##TRAINING BASED ON DIVISION ACCORDING TO TIME.
# Include also lags as input variables
# Tuning parameters SVM
tune_sigma<-seq(0.001,2,(2-0.001)/50)
tune_ww<-c(5)
crossval_results1<-matrix(NA,length(tune_ww), length(tune_sigma))

for(ii in 1:length(tune_ww)){
  ## Load data
  print(ii)
  TM<-na.omit(fread("TM21.csv"))
  
  ## Prepare data
  colnames(TM)<-toupper(colnames(TM))
  vars_selected<-c("GAS", "LUZ", "TEMPERATURA", "HUMEDAD")
  secs_interval<-60*tune_ww[ii] #4 minutes=300 sec
  x_initial<-min(as.POSIXlt(TM$FECHA, format="%d-%m-%y %H:%M"))
  x_final<-max(as.POSIXlt(TM$FECHA, format="%d-%m-%y %H:%M"))
  x_interval<-seq(from=x_initial,to=x_final+2*secs_interval, by=secs_interval) 
  x_interval_groups<-unlist(lapply(1:dim(TM)[1], function(x) which(as.POSIXlt(TM[x,FECHA],format="%d-%m-%y %H:%M")<=x_interval)[1]))
  
  TM$FECHA_INTERVALO<-x_interval[x_interval_groups]
  TM<-TM[, lapply(.SD, mean), by=FECHA_INTERVALO, .SDcols=vars_selected]
  TM$HORA<-hour(as.ITime(as.POSIXlt(TM$FECHA_INTERVALO)))
  TM[,c("JORNADA"):=list(ifelse(HORA<6|HORA>18, "NOCHE",ifelse(HORA>=6&HORA<=12,"MANANA", "TARDE")))]
  TM[,ID:=as.character(1:.N)]
  fecha_sort<-sort.int(TM$FECHA_INTERVALO, index.return = T)
  TM<-TM[fecha_sort$ix]
  
  ## Divide training and test data
  
  id_train<-round(dim(TM)[1]*0.7)
  plot(TM[, FECHA_INTERVALO], TM[,GAS], "l")
  lines(TM[id_train:.N, FECHA_INTERVALO], TM[id_train:.N,GAS], col="red")
  media_train<-mean(TM[1:id_train, GAS])
  sd_train<-sd(TM[1:id_train, GAS])
  TM_TRAIN2<-TM[1:id_train,lapply(.SD, function(x) (x-media_train)/sd_train),.SDcols=c(vars_selected,"HORA")]
  TM_TEST2<-TM[(id_train+1):.N,lapply(.SD, function(x) (x-media_train)/sd_train),.SDcols=c(vars_selected,"HORA")]
  TM2<-rbind(data.table(TM_TRAIN2, TYPE="TRAIN"), data.table(TM_TEST2, TYPE="TEST"))
  nlags=30
  TM2<-na.omit(TM2[, unlist(lapply(1:nlags, function(x) paste0("GAS",x))):=shift(GAS,1:nlags,type="lag")])
  TM_TRAIN2<-TM2[TYPE=="TRAIN"]
  TM_TEST2<-TM2[TYPE=="TEST"]
  TM_TRAIN2[, TYPE:=NULL]
  TM_TEST2[,TYPE:=NULL]
  
  ## Fit Model
  for(jj in 1:length(tune_sigma)){
    print(jj)
    #model<-train(GAS ~ ., TM_TRAIN2,method = "brnn",
    #             trControl = trainControl(
    #               method = "cv", number =10,
    #               repeats = 1, verboseIter = TRUE),
    #             tuneGrid=expand.grid(neurons=10)
    #)
    #model<-lm(GAS~., data = TM_TRAIN2)
    model<-gausspr(GAS ~ ., data=TM_TRAIN2, type="regression", 
                   kernel="rbfdot",kpar=list(sigma=tune_sigma[jj]), 
                   variance.model=F, cross=5) 
    model
    #crossval_results[ii,jj]<-model@cross
    #p<-predict(model,TM_TRAIN2)
    #plot(TM_TRAIN2$GAS,p);sqrt(mean((TM_TRAIN2$GAS-p)^2))
    #p1<-predict(model,TM_TEST2)
    #plot(TM_TEST2$GAS,p1);sqrt(mean((TM_TEST2$GAS-p1)^2))
    
    
    
  }
}




##########################################################
## KERNEL LAB. SVM 


model<-gausspr(GAS ~ ., data=TM_TRAIN2, type="regression", 
        kernel="rbfdot",kpar=list(sigma=0.36), 
        variance.model=F, cross=5) 
model
p<-predict(model,TM_TRAIN2)
plot(TM_TRAIN2$GAS,p);(mean((TM_TRAIN2$GAS-p)^2))
p1<-predict(model,TM_TEST2)
plot(TM_TEST2$GAS,p1);(mean((TM_TEST2$GAS-p1)^2))


plot(TM_TRAIN2[,GAS]*sd_train+media_train,type="l")
lines(p*sd_train+media_train,type="l", col="red")
plot(TM_TEST2[,GAS],type="l")
lines(p1,type="l", col="red")


plot(TM[(nlags+1):.N,GAS], type="l")
lines(c(p,p1)*sd_train+media_train,type="l", col="red")


##########################################################
## Explorar para outliers 


TM_TRAIN2<-TM_TRAIN[,.(GASm=(GASm-mean(GASm))/sd(GASm),
            HUMm=(HUMm-mean(HUMm))/sd(HUMm),
            TEMPm=(TEMPm-mean(TEMPm))/sd(TEMPm),
            LUZm=(LUZm-mean(LUZm))/sd(LUZm),
            HORA=(HORA-mean(HORA))/sd(HORA))]

TM_VAL2<-TM_VAL[,.(GASm=(GASm-mean(GASm))/sd(GASm),
                       HUMm=(HUMm-mean(HUMm))/sd(HUMm),
                       TEMPm=(TEMPm-mean(TEMPm))/sd(TEMPm),
                       LUZm=(LUZm-mean(LUZm))/sd(LUZm),
                       HORA=(HORA-mean(HORA))/sd(HORA))]

mod<-lm(GASm~LUZm+HUMm+TEMPm+HORA+
             LUZm*HUMm+LUZm*TEMPm+LUZm*HORA+
             HUMm*TEMPm+HUMm*HORA+
             TEMPm*HORA+
             I(LUZm^2)+I(HUMm^2)+I(TEMPm^2)+I(HORA^2), data=TM_TRAIN2)
summary(mod)
summary(step(mod))
boxplot(residuals(mod))
plot(fitted(mod), TM_TRAIN2$GASm)
sqrt(mean(residuals(mod)^2))


library(caret)
library(neuralnet)

library(robust)

# Detection of outliers MCD estimator


TM_TRAIN1<-TM_TRAIN[,.SD,.SDcols=c("GASm","HUMm","LUZm", "HORA")]
mcd_train<-covRob(TM_TRAIN1, estim="mcd")
mcd_train<-list()
mcd_train$center<-apply(TM_TRAIN1,2,mean)
mcd_train$cov<-cov(TM_TRAIN1)

Y=(TM_TRAIN1-matrix(mcd_train$center,dim(TM_TRAIN1)[1],dim(TM_TRAIN1)[2] ,byrow=T))
Dis<-sqrt(diag(as.matrix(Y)%*%mcd_train$cov%*%as.matrix(t(Y))))
sum(Dis>sqrt(qchisq(0.95,4)))


# Fit  model using 10 x 1-fold CV: NN

model<-train(GASm ~ ., TM_TRAIN2,method = "neuralnet",
             trControl = trainControl(
                          method = "cv", number =10,
                          repeats = 1, verboseIter = TRUE),
             tuneGrid=expand.grid(layer1=c(5), layer2=0,layer3=0)
            )
model
p<-predict(model,TM_TRAIN2)
plot(TM_TRAIN2$GASm,p)
boxplot(residuals(model))
p1<-predict(model,TM_VAL2)
plot(TM_VAL2$GASm,p1)
?neuralnet



