# Exercise 2 

## Option A (Pimp my project)

_**Disclaimer**: This exercise poses a **risk of data loss** as it asks the user to work on a copied version of one of the user's local folders. The instructors do not hold any responsibility for data loss if the user mistakenly deletes or replaces local files. This exercise is entirely done locally by the user and the user will not need to share any personal files or folders with the instructors_

It's time for you get to evaluate your own projects. How reproducible have you been so far?
In this exercise, you're gonna discover how reproducible one of your own projects is and how you can make more reproducible if desired. In this case, **you are the user of your own project**. 

What you're going to get here:

- A numeric value of how reproducible your project was
- An enhanced version of your project for the sake of reproducibility
- An understanding of the causes of lack of reproducibility of your project

Are you ready? Let's go!

### Instructions:

1. Pick a project of your own (e.g. from your local computer) where you have obtained in the past an outut piece such as a figure, a table, or any other type of output as the result of your project. This can be one output piece or multiple output pieces. It's your choice depending on what you need to reproduce. Localize the **path to the output piece(s)** and localize **the path of the parent directory of this project**.

_**The user** of this project is **yourself**. As a user, everything that you need is to be able to reproduce the output piece(s)_

2. Create a **copy** of your project folder (parent directory).

You are going to turn it into a more reproducible project for the purpose of obtaining the output piece(s). Note that you might not need to change anything or almost anything if your project is already reproducible. **Rename your copied project differently enough so you don't risk modifying your existing folder**. We'll call this copy the **reproducible project**.

3. In the copied **reproducible project**, start arranging folders and files so that you comply with the structure of **input &rarr; process &rarr; output**. Some aspects to keep in mind are: 
    - The first folders and files that you should find when you open a project folder should be related to input, process, output, and documentation.
    - You might find that there are files living out of the parent folder that need to be used in your project. Do they need to live inside your project? If so, bring them in by **copying them in your reproducible project**. 
    - Most likely, some folders or files are not necessary to reproduce the output piece(s). Transfer them to another location or rename them starting with ".". 
    - Some dependencies (other folders, input data stored remotely, etc.) might not live in the reproducible project because of size or accessibility. Is this documented? Is there currently a documentation file, or should you create it now?
    - If you're not able to comply with the structure of **input, process, output**, can you explain why? Can you still have a defined structure?
    - If you have multiple output pieces, were they originally living in the same subfolder? If not, can you make a unique output folder?
    - Where there any manual steps involved? Are they documented?
    - How else can you improve the reproducibility of your project?

You made it! Now you have a **reproducible project**.

3. Obtain the diagrama of the **original** and the **reproducible** project. [Follow the instructions here to obtain the diagram](www.gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises/utilities/get_diagram.md)

4. Quantify the similarity between the **original** and the **reproducible** project. [Follow the instructions here to obtain the metrics](www.gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises/utilities/get_metric.md)

_Congratulations! Are you now reproducible?_

## Option B (What a mess!)

Welcome to the real messy project of some wifi measurements to predict several environmental parameters such as humidity, temperature, etc.
This is a real project that was developed a looong time ago. Now, many years after the analysis was done, you are the user and you need to obtain the same output that was delivered back then. 

The **input** of this project was some data containing time series of the different environmental variables. The **output** was a prediction model and the setup of a web application to use that model. The process was run in R. 


- Can you just figure out the path that led from the **input** data to the **output** model?
- Where are the input files?
- Where is the output? (model and app)
- Are there only R scripts?
- Is there any documentation file?

### Instructions

Your mission, if you accept it, is :

1. Re organize this messy project folder in a way that the project makes sense for reproducibility. For this, you will follow the principles of **input**, **process**, and **output**. You can delete all the files that you don't think should belong to this project. There's no risk of data loss here, so don't be afraid, just be smart so your final folder makes sense. 

2. Obtain the diagrama of the **original** and the **reproducible** project. [Follow the instructions here to obtain the diagram](www.gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises/utilities/get_diagram.md)

3. Quantify the similarity between the **original** and the **reproducible** project. [Follow the instructions here to obtain the metrics](www.gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises/utilities/get_metric.md)


_Congratulations! Are you now reproducible?_