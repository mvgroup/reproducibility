#!/usr/bin/env python
# coding: utf-8

# In[1]:



from matplotlib import pyplot as plt
import numpy as np
import pandas as pd


# # Analysis of platforms

# In[2]:


keyterms_nodata = ["github", "gitlab", "zenodo","figshare", 
            "osf","data.mendeley", "vizier"]


# In[3]:


df = pd.read_csv("./pubmed_summary_df.csv").drop("Unnamed: 0", axis = 1)


# In[4]:


df


# In[5]:


# counts of each key term per year (line plots where every line is a keyterm)

df_papers_year_plt = df.loc[:,["id","pub_year_num"]].groupby(["pub_year_num"]).count()
df_papers_year_plt.reset_index(inplace=True)
df_papers_year_plt["total_per_year"] = df_papers_year_plt["id"]


# ## Analysis Data availability

# In[6]:


df_da = df.loc[:,["id","<title>data availability" , "pub_year"]]
df_da["pub_year_num"] = df_da["pub_year"].astype(np.int32)
df_da["data availability"] = df_da["<title>data availability"]
df_da


# In[7]:


df_da_count = df_da.groupby("pub_year_num").sum().reset_index()


plt.rcParams.update({'font.size': 20})
fig, ax = plt.subplots(figsize = (12,8))
ax.bar(df_da_count["pub_year_num"], df_da_count["data availability"], linewidth = 3)
ax.set_title('Data availability: number of articles per year')
ax.set_xticks(ticks = df_da_count["pub_year_num"])
ax.set_xticklabels(labels = df_da_count["pub_year_num"], rotation = 40)
ax.set_ylabel('Number of units')
ax.set_xlabel('Publication year')
plt.show()


df_da_count_rel = df_da_count.merge(df_papers_year_plt, on = "pub_year_num", how = "left")
df_da_count_rel["value_relative"] = df_da_count_rel["data availability"] / df_da_count_rel["total_per_year"]
df_da_count_rel["value_percentage"] = 100*df_da_count_rel["value_relative"] 


plt.rcParams.update({'font.size': 20})
fig, ax = plt.subplots(figsize = (12,8))
ax.bar(df_da_count_rel["pub_year_num"], df_da_count_rel["value_percentage"], linewidth = 3)
ax.set_title('Data availability: % of articles per year')
ax.set_xticks(ticks = df_da_count_rel["pub_year_num"])
ax.set_xticklabels(labels = df_da_count_rel["pub_year_num"], rotation = 40)
ax.set_ylabel('% units per year')
ax.set_xlabel('Publication year')

plt.show()


# ## Analysis sources

# In[8]:


# counts of each key term


plt.rcParams.update({'font.size': 20})
fig, ax = plt.subplots(figsize = (12,8))
ax.bar(keyterms_nodata, df[keyterms_nodata].sum())
ax.set_title('Keyword instances in {} papers found'.format(df.shape[0]))#, search_term))
ax.set_xticks(ticks = keyterms_nodata)
ax.set_xticklabels(labels = keyterms_nodata, rotation = 40)
ax.set_ylabel('Instances for keyword')
ax.set_xlabel('Keyword')
plt.show()


# In[9]:





plt.rcParams.update({'font.size': 20})
fig, ax = plt.subplots(figsize = (12,8))
ax.bar(df_papers_year_plt["pub_year_num"], df_papers_year_plt["total_per_year"])
ax.set_title('Distribution of number of articles per year')
ax.set_xticks(ticks = df_papers_year_plt["pub_year_num"])
ax.set_xticklabels(labels = df_papers_year_plt["pub_year_num"], rotation = 40)
ax.set_ylabel('Number of units')
ax.set_xlabel('Publication year')
plt.show()


# In[10]:


# counts of each key term per year (line plots where every line is a keyterm)

df_noda = df.drop("<title>data availability", axis = 1)
df_term_year = pd.melt(df_noda, id_vars =["id","pub_year","pub_year_num"])
df_term_year_plt = df_term_year.groupby(["variable","pub_year_num"]).sum()
df_term_year_plt.reset_index(inplace=True)

plt.rcParams.update({'font.size': 22})
fig, ax = plt.subplots(figsize = (20,8))
for label, grp in df_term_year_plt.groupby(["variable"]):
    ax.plot(grp["pub_year_num"], grp["value"], linewidth = 4, label = label)
ax.set_xticks(np.sort(np.unique(df["pub_year_num"])))
ax.set_xticklabels(labels = np.sort(np.unique(df["pub_year_num"])), rotation = 40)
ax.set_xlabel("Publication year")
ax.set_ylabel("Number of entries")
ax.set_title("Counts of sources per year")
ax.legend()
plt.show()


# In[11]:


# relative counts of each key term per year (line plots where every line is a keyterm)

df_term_year_rel = df_term_year_plt.merge(df_papers_year_plt, on = "pub_year_num", how = "left")
df_term_year_rel["value_relative"] = df_term_year_rel["value"] / df_term_year_rel["total_per_year"]

plt.rcParams.update({'font.size': 22})
fig, ax = plt.subplots(figsize = (20,8))
for label, grp in df_term_year_rel.groupby(["variable"]):
    ax.plot(grp["pub_year_num"], grp["value_relative"]*100, linewidth = 3, label = label)
ax.set_xticks(np.sort(np.unique(df["pub_year_num"])))
ax.set_xticklabels(labels = np.sort(np.unique(df["pub_year_num"])), rotation = 40)
ax.set_xlabel("Publication year")
ax.set_ylabel("% of entries")
ax.set_title("% of sources per year")
ax.legend()
plt.show()


# In[12]:


# relative cumulative sum per term



plt.rcParams.update({'font.size': 22})
fig, ax = plt.subplots(figsize = (20,8))
for label, grp in df_term_year_rel.groupby(["variable"]):
    ax.plot(grp["pub_year_num"], np.cumsum(grp["value_relative"])*100, linewidth = 2, label = label)
ax.set_xticks(np.sort(np.unique(df["pub_year_num"])))
ax.set_xticklabels(labels = np.sort(np.unique(df["pub_year_num"])), rotation = 40)
ax.set_xlabel("Publication year")
ax.set_ylabel("% of entries")
ax.set_title("% of sources per year")
ax.legend()
plt.show()


# In[ ]:





# In[ ]:




