## Exercise 1

> Can you reproduce a project ...

1. when it is a **completely** **automatic** process?

2. when it has a **few manual** steps?

3. when it is **completely manual**?

Let's reproduce! [(Click here)](https://gitlab.com/mvgroup/reproducibility_workshop/-/tree/main/exercises)

