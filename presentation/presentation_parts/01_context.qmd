<!-- ## Reproducibility:  -->
<!-- > to repeat a result -->

## Reproducibility: 
> To recreate an idea or process

:::: {.columns}

::: {.column width="50%"}
[![](assets/images/vaccine1.png){width="80%"}](http://www.phrma-jp.org/wordpress/wp-content/uploads/old/library/vaccine-factbook_e/1_Basic_Concept_of_Vaccination.pdf)
:::

::: {.column width="50%"}
[![](assets/images/vaccine2.png)](https://en.wikipedia.org/wiki/COVID-19_vaccine)
:::

::::

## Is there a reproducibility crisis in science?

:::: {.columns}
::: {.column width="50%"}
[![](assets/images/showme_trustme.png)](https://www.nature.com/articles/d41586-019-00067-3)
:::
::: {.column width="50%"}
```{=html}
<iframe width="100%" height="500" src="https://www.youtube.com/embed/j7K3s_vi_1Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```
:::
::::

## The real issue behind the reproducibility crisis? {transition="convex"}

::: {}
[![](assets/images/foco-em-resultados.jpg)](https://www.ibccoaching.com.br/portal/metas-e-objetivos/foco-em-resultados-competencia-essencial-seu-negocio/)
:::

::: {.fragment .fade-up}
Only focus on production of results
:::

## Reproducibility problem: {.smaller}
> Academia Edition 

:::: {.columns}

::: {.column width="60%"}
![](assets/images/academy.png)
:::

::: {.column width="40%"}
::: {.fragment}
- No time
- Publication pressure
- Poor organization
- Result fishing
- Lack of ideas
- No will to learn new skills
:::

::: {.fragment}
[{{< fa thumbs-down >}} Undermine scientific research credibility!]{style="color:#002562"}
:::
:::
::::

## Reproducibility problem: {.smaller}
> Industry Edition 

:::: {.columns}

::: {.column width="60%"}
![](assets/images/industry.png)
:::

::: {.column width="40%"}
::: {.fragment}
- No time
- Task completion pressure
- Lack of planning beforehand
- Non-structured work
- Competititve focus on growth
- Lack of communication
:::
::: {.fragment}
Risk of:

- [{{< fa circle-dollar-to-slot >}} Inefficiency]{style="color:#002562"}
- [{{< fa clock-rotate-left >}} Innovation slow-down]{style="color:#002562"}
- [{{< fa user-shield >}} Lack of accountability]{style="color:#002562"}

:::
:::
::::

## How is it in real life?

::: {.r-stack}
::: {.fragment}
::: {.fragment .fade-out}
Academia Edition

![](assets/images/academy_sketch.png)
:::
:::
::: {.fragment}
Industry Edition

![](assets/images/industry_sketch.png)
:::
:::

## Factors of the reproducibility problem {auto-animate=true transition="convex" .center}

Failure to reproduce results

Small differences in execution

Selective reporting

Detractors of execution

## Factors of the reproducibility problem {auto-animate=true .center}

[Failure to reproduce results]{style="color:#002562;text-decoration:underline"}

{{< fa magnifying-glass >}} In your own research 

::: {.fragment}
{{< fa book >}} Results in a paper you found 
:::

::: {.fragment}
{{< fa people-group >}} Results from a previous collaboration 
:::

## Factors of the reproducibility problem {auto-animate=true transition="convex" .center}

Failure to reproduce results

Small differences in execution

Selective reporting

Detractors of execution

## Factors of the reproducibility problem {auto-animate=true .center}

[Small differences in execution]{style="color:#002562;text-decoration:underline"}

{{< fa vials >}} Steps in physical experiments

::: {.fragment}
{{< fa computer >}} Randomization in computational experiments
:::

::: {.fragment}
{{< fa arrow-up-1-9 >}} Changes in software version
:::

## Factors of the reproducibility problem {auto-animate=true transition="convex" .center}

Failure to reproduce results

Small differences in execution

Selective reporting

Detractors of execution

## Factors of the reproducibility problem{auto-animate=true .center}

[Selective reporting]{style="color:#002562;text-decoration:underline"}

{{< fa gears >}} Difficult parameter tuning in modelling

::: {.fragment}
{{< fa database >}} Difficult data processing
:::

::: {.fragment}
{{< fa microchip >}} Deep data selection
:::

## Factors of the reproducibility problem {auto-animate=true transition="convex" .center}

Failure to reproduce results

Small differences in execution

Selective reporting

Detractors of execution

## Factors of the reproducibility problem{auto-animate=true .center}

[Detractors of execution]{style="color:#002562;text-decoration:underline"}

{{< fa clock >}} Lack of time

::: {.fragment}
{{< fa recycle >}} Incomplete release of material
:::

::: {.fragment}
{{< fa newspaper >}} Publication pressure
:::

::: {.fragment}
{{< fa calendar >}} Lack of organization
:::

::: {.fragment}
{{< fa kitchen-set >}} Lack of interest to learn new skills
:::

